<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Company</title>
    <link rel="stylesheet" href="/css/company/index.css">
</head>

<body>
    <div class="card">
        <h3>List Company</h3>
        @if ($message = Session::get('success'))
        <div class="info">
            {{$message}}
        </div>
        @endif
        <table>
            <thead>
                <tr>
                    <th>no</th>
                    <th>nama</th>
                    <th>alamat</th>
                    <th>aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($company as $no => $item)
                <tr>
                    <td>{{ $no+1 }}.</td>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->alamat }}</td>
                    <td>
                        <a href="{{ route('company.edit', $item->id) }}" class="btn btn-edit">Edit</a>
                        <form action="{{ route('company.destroy', $item->id) }}" method="POST">
                            @csrf
                            <input type="hidden" name="_method" value="delete" />
                            <input type="submit" class="btn btn-hapus" value="Hapus">
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <a href="{{ route('company.create') }}" class="btn btn-tambah">Tambah</a>
    </div>
</body>

</html>