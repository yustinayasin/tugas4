<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Company</title>
    <link rel="stylesheet" href="/css/company/edit.css">
</head>

<body>
    <form action="{{ route('company.update',$company->id) }}" method="POST">
        <h1>Edit Company</h1>
        @csrf
        @method('put')
        <label for="nama">Nama :</label>
        <input type="text" id="nama" name="nama" value="{{$company->nama}}" />
        <label for="">Alamat</label>
        <input type="text" name="alamat" placeholder="Alamat" value="{{$company->alamat}}">
        <div>
            <a href="{{ route('company.index') }}" class="btn btn-back">Back</a>
            <button type="submit" class="btn btn-simpan" value="simpan">Simpan</button>
        </div>
    </form>
</body>

</html>