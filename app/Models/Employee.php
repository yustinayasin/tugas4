<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employee';
    protected $fillable = [
        'nama',
        'atasan_id',
        'company_id'
    ];

    // public function present_nama_atasan(){
    //     return $this->find($this->atasan_id)->nama;
    // }
    // public function present_nama_company(){
    //     $company = $this->company;
    //     return $company->nama;
    // }
    public function bawahan(){
        return $this->hasMany(self::class,'atasan_id');
    }
    public function atasan(){
        return $this->belongsTo(self::class,'atasan_id');
    }
    public function company(){
        return $this->BelongsTo('App\Models\Company','company_id');
    }
}
